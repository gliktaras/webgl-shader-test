# WebGL Shader Test

Experiments with shaders in WebGL. If Javascript does not work, make sure ES6
modules are enabled in your web browser.

Resources followed:

*   [MDN WebGL tutorial](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Getting_started_with_WebGL).
*   [The Book of Shaders](https://thebookofshaders.com/).

## Credits

*   Pavement texture by texturelib.com
    ([link](http://texturelib.com/texture/?path=/Textures/brick/pavement/brick_pavement_0105)).

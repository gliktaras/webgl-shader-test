function createShader(gl, type, source) {
  const shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);

  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    console.log('Shader compilation error: ' + gl.getShaderInfoLog(shader));
    return null;
  }
  return shader;
}

export function createShaderProgram(gl, vertexShaderSource, fragmentShaderSource) {
  const vertexShader =
    createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
  const fragmentShader =
    createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
  if (!vertexShader || !fragmentShader) {
    return null;
  }

  const program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);

  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    console.log('Program linking error: ' + gl.getProgramInfoLog(program));
    return null;
  }

  return {
    program: program,
    attribLocations: {},
    uniformLocations: {
      mouseAbsolute: gl.getUniformLocation(program, 'uMouseAbs'),
      mouseRelative: gl.getUniformLocation(program, 'uMouseRel'),
      resolution: gl.getUniformLocation(program, 'uResolution'),
      timeMillis: gl.getUniformLocation(program, 'uTimeMillis'),
    },
  };
}

export function loadTexture(gl, url) {
  const texture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, texture);
  gl.texImage2D(
    gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
    new Uint8Array([0, 0, 255, 255]));

  let image = new Image();
  image.onload = function () {
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  };
  image.src = url;

  return texture;
}

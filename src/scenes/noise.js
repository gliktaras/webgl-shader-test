// Chapter 11 from the Book of Shaders (at https://thebookofshaders.com/11/).

import {createShaderProgram} from '../glUtils.js';

const PATTERN_TYPE = {
  'ROLLING_HILLS': 1,
  'WARPED_PLANE': 2,
  'ROTHKO': 3,
  'WOOD_SHAVINGS': 4,
  'WOOD': 5,
  'BLUE_MINERAL': 6,
  'RIPPLE': 7,
  'BEE': 8,
};

const FRAGMENT_SHADER_SOURCE = `
  precision mediump float;

  const float PI = 3.14159265359;
  const float TWO_PI = 6.28318530718;
  const float SEED = PI * PI * 2189.14641;

  uniform vec2 uMouseRel;
  uniform vec2 uResolution;
  uniform float uTimeMillis;

  uniform int uPattern;

  float random(vec2 pos, float seed) {
    return fract(sin(dot(pos, vec2(123.456789, 987.654321))) * seed);
  }

  float random(float value, float seed) {
    return random(vec2(value, 0.0), seed);
  }

  vec2 random2d(vec2 pos, float seed) {
    vec2 v = vec2(dot(pos, vec2(127.721127, 413.314413)),
                  dot(pos, vec2(365.563365, 158.851158)));
    return -1.0 + 2.0 * fract(sin(v) * seed);
  }

  float truncate(float x, float f) {
    return floor(x * f) / f;
  }

  vec2 rotate(float theta, vec2 center, vec2 pos) {
    mat2 TRANSFORM = mat2(cos(theta), sin(theta), -sin(theta), cos(theta));
    return TRANSFORM * (pos - center) + center;
  }

  float valueNoiseCurve(float segment, float seed) {
    float sIndex = floor(segment) + 10.0;
    float sPos = fract(segment);
    float yPrev = random(sIndex, seed);
    float yNext = random(sIndex + 1.0, seed);
    float yWeight = smoothstep(0.0, 1.0, sPos);
    return mix(yPrev, yNext, yWeight);
  }

  float valueNoiseCurveMix(float segment, float tStep, float tPos) {
    float yPrev = valueNoiseCurve(segment, SEED + tStep);
    float yNext = valueNoiseCurve(segment, SEED + tStep + 1.0);
    float w = smoothstep(0.0, 1.0, tPos);
    return mix(yPrev, yNext, w);
  }

  vec4 rollingHills() {
    const vec4 FG_COLOR = vec4(0.004, 0.039, 0.239, 1.0);
    const vec4 BG_COLOR = vec4(0.882, 0.996, 0.643, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    pos.y = 1.0 - pos.y;
    float t = uTimeMillis / 1000.0;

    float b = floor(pos.y * 10.0);
    float tB = t * (b * 0.3 + 1.0);
    float tStep = floor(tB);
    float tPos = fract(tB);

    float s = pos.x * (20.0 - b * 1.2) + t * (b + 1.0);
    float y = valueNoiseCurveMix(s, tStep + b * PI, tPos);
    y = y * 0.8 + 0.1;
    float w = (b + 2.0 - step(pos.y, (b + y) / 10.0)) / 11.0;

    return mix(FG_COLOR, BG_COLOR, w);
  }

  float valueNoisePlane(vec2 pos, float seed) {
    vec2 gridPos = floor(pos);
    vec2 cellPos = smoothstep(0.0, 1.0, fract(pos));

    float v1 = random(gridPos + vec2(0.0, 0.0), seed);
    float v2 = random(gridPos + vec2(1.0, 0.0), seed);
    float v3 = random(gridPos + vec2(0.0, 1.0), seed);
    float v4 = random(gridPos + vec2(1.0, 1.0), seed);

    return v1 * (1.0 - cellPos.x) * (1.0 - cellPos.y) +
           v2 * cellPos.x * (1.0 - cellPos.y) +
           v3 * (1.0 - cellPos.x) * cellPos.y +
           v4 * cellPos.x * cellPos.y;
  }

  vec2 badGradientNoisePlane(vec2 pos, float seed) {
    vec2 gridPos = floor(pos);
    vec2 cellPos = smoothstep(0.0, 1.0, fract(pos));

    vec2 v1 = random2d(gridPos + vec2(0.0, 0.0), seed);
    vec2 v2 = random2d(gridPos + vec2(1.0, 0.0), seed);
    vec2 v3 = random2d(gridPos + vec2(0.0, 1.0), seed);
    vec2 v4 = random2d(gridPos + vec2(1.0, 1.0), seed);

    return mix(mix(v1, v2, cellPos.x), mix(v3, v4, cellPos.x), cellPos.y);
  }

  float gradientNoisePlane(vec2 pos, float seed) {
    vec2 gridPos = floor(pos);
    vec2 cellPos = fract(pos);
    vec2 smoothCellPos = smoothstep(0.0, 1.0, cellPos);

    vec2 r1 = random2d(gridPos + vec2(0.0, 0.0), seed);
    vec2 r2 = random2d(gridPos + vec2(1.0, 0.0), seed);
    vec2 r3 = random2d(gridPos + vec2(0.0, 1.0), seed);
    vec2 r4 = random2d(gridPos + vec2(1.0, 1.0), seed);
    vec2 c1 = cellPos - vec2(0.0, 0.0);
    vec2 c2 = cellPos - vec2(1.0, 0.0);
    vec2 c3 = cellPos - vec2(0.0, 1.0);
    vec2 c4 = cellPos - vec2(1.0, 1.0);

    return mix(mix(dot(r1, c1), dot(r2, c2), smoothCellPos.x),
               mix(dot(r3, c3), dot(r4, c4), smoothCellPos.x),
               smoothCellPos.y);
  }

  vec4 warpedPlane() {
    const vec4 LOW_COLOR = vec4(0.518, 0.157, 0.357, 1.0);
    const vec4 HIGH_COLOR = vec4(0.937, 0.929, 0.961, 1.0);

    const float GRID_SIZE = 40.0;
    const float WARP_SIZE = 4.0;
    const float CIRCLE_SIZE = 0.5;
    const float DENSITY = 5.0;

    vec2 pos = gl_FragCoord.xy / uResolution;
    pos.y = 1.0 - pos.y;
    float tStep = floor(uTimeMillis / 1000.0);

    vec2 warpedPlanePos = pos * GRID_SIZE;
    float w = valueNoisePlane(warpedPlanePos, SEED + tStep);
    w *= min(length(pos - uMouseRel) * WARP_SIZE - CIRCLE_SIZE, 1.0);
    w = step(mod(w * DENSITY, 1.0), 0.5);

    return mix(LOW_COLOR, HIGH_COLOR, w);
  }

  vec4 rothko() {
    const vec4 BLACK_COLOR_1 = vec4(0.204, 0.196, 0.239, 1.0);
    const vec4 BLACK_COLOR_2 = vec4(0.227, 0.220, 0.263, 1.0);
    const vec4 PINK_COLOR_1 = vec4(0.914, 0.549, 0.553, 1.0);
    const vec4 PINK_COLOR_2 = vec4(0.884, 0.517, 0.532, 1.0);
    const vec4 WHITE_COLOR_1 = vec4(0.973, 0.882, 0.914, 1.0);
    const vec4 WHITE_COLOR_2 = vec4(1.000, 0.910, 0.953, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    pos.y = 1.0 - pos.y;

    float seed = SEED + uTimeMillis / 10000.0;
    float pinkColorW = valueNoisePlane(pos * vec2(300.0, 5.0), seed);
    float rColorW = valueNoisePlane(pos * 200.0, seed);
    float maskColorW = valueNoisePlane(pos * 50.0, seed);

    vec2 blackPos = abs(pos - vec2(0.5, 0.325));
    blackPos = blackPos - vec2(0.4, 0.225);
    float blackLimit = max(blackPos.x, blackPos.y) / 0.05;
    blackLimit = clamp(blackLimit, 0.0, 1.0);
    float blackW = smoothstep(0.0, maskColorW, blackLimit);
    blackW = 1.0 - pow(blackW, 10.0);

    vec2 whitePos = abs(pos - vec2(0.5, 0.765));
    whitePos = whitePos - vec2(0.4, 0.135);
    float whiteLimit = max(whitePos.x, whitePos.y) / 0.05;
    whiteLimit = clamp(whiteLimit, 0.0, 1.0);
    float whiteW = smoothstep(0.0, maskColorW, whiteLimit);
    whiteW = 1.0 - pow(whiteW, 10.0);

    float pinkW = 1.0 - blackW - whiteW;

    return mix(PINK_COLOR_1, PINK_COLOR_2, pinkColorW) * pinkW
        + mix(BLACK_COLOR_1, BLACK_COLOR_2, rColorW) * blackW
        + mix(WHITE_COLOR_1, WHITE_COLOR_2, rColorW) * whiteW;
  }

  vec4 woodShavings() {
    const vec4 COLOR1 = vec4(0.769, 0.690, 0.655, 1.0);
    const vec4 COLOR2 = vec4(0.973, 0.886, 0.765, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;

    vec2 absPos = pos * vec2(30.0);
    vec2 g = badGradientNoisePlane(absPos, SEED + floor(uTimeMillis / 1000.0));
    float angle = atan(g.y, g.x) / (TWO_PI * 16.0);

    vec2 linePos = rotate(angle, vec2(-20.0), absPos);
    float w = step(mod(linePos.y, 1.0), 0.5);

    return mix(COLOR1, COLOR2, w);
  }

  vec4 wood() {
    const vec4 COLOR1 = vec4(0.671, 0.608, 0.518, 1.0);
    const vec4 COLOR2 = vec4(0.949, 0.886, 0.796, 1.0);
    vec2 pos = gl_FragCoord.xy / uResolution;

    vec2 absPos = pos * vec2(2.0, 6.0) + vec2(0.0, 2.0);
    float angle = gradientNoisePlane(absPos, SEED + floor(uTimeMillis / 1000.0));
    angle = angle * PI / 4.0;

    vec2 linePos = rotate(angle, vec2(0.0), absPos);
    float w = mod(linePos.y * 4.0, 1.0);
    w = smoothstep(0.0, 0.5, w) - smoothstep(0.5, 1.0, w);
    w = 1.0 - clamp(pow(w, 3.0) * 1.5, 0.0, 1.0);

    return mix(COLOR1, COLOR2, w);
  }

  vec4 blueMineral() {
    const vec4 BLUE_HIGH = vec4(0.812, 0.933, 0.988, 1.0);
    const vec4 BLUE_LOW = vec4(0.000, 0.369, 0.475, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;

    vec2 absPos = pos * vec2(3.0) + vec2(10.0);
    float angle = gradientNoisePlane(absPos, SEED + floor(uTimeMillis / 1000.0));
    angle = angle * TWO_PI - PI;
    vec2 linePos = rotate(angle, vec2(0.0), absPos);

    float w = abs(gradientNoisePlane(linePos, SEED));
    w = clamp(0.0, 1.0, w);
    return mix(BLUE_LOW, BLUE_HIGH, w);
  }

  vec4 ripple() {
    const vec4 HIGH_COLOR = vec4(0.969, 0.992, 0.984, 1.0);
    const vec4 LOW_COLOR = vec4(0.082, 0.816, 0.945, 1.0);

    const int RIPPLE_DENSITY = 8;
    const float RIPPLE_LINE_WIDTH = 0.005;
    const float RIPPLE_WIDTH = 0.1;

    vec2 pos = gl_FragCoord.xy / uResolution;

    vec2 cPos = pos - vec2(0.5);
    float dist = length(cPos);
    float angle = atan(cPos.y, cPos.x);

    vec2 noisePos = cPos * 30.0 + uTimeMillis / 1000.0;
    float x = gradientNoisePlane(noisePos, SEED);
    x = sin(x) * 0.5;

    float w = 0.0;
    for (int i = 0; i < RIPPLE_DENSITY; i++) {
      float t = fract(uTimeMillis / 10000.0);
      t = mod(t + float(i) / float(RIPPLE_DENSITY), 1.0) + x * RIPPLE_WIDTH;
      t = clamp(0.0, 1.0, t);
      w += step(abs(dist - t), RIPPLE_LINE_WIDTH);
    }

    return mix(LOW_COLOR, HIGH_COLOR, w);
  }

  vec4 bee() {
    const vec4 BG_COLOR = vec4(0.400, 0.518, 0.008, 1.0);
    const vec4 STRIPE_1_COLOR = vec4(1.000, 0.961, 0.000, 1.0);
    const vec4 STRIPE_2_COLOR = vec4(0.161, 0.129, 0.047, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    float offset = uTimeMillis / 1000.0;

    float x = gradientNoisePlane(vec2(1.0) + vec2(offset, 0.0), SEED);
    float y = gradientNoisePlane(vec2(1.0) + vec2(0.0, offset), SEED);
    vec2 center = vec2(x, y) + vec2(0.5);
    vec2 cPos = pos - center;

    float beeW = step(length(cPos), 0.052);
    float bgW = 1.0 - beeW;
    float stripeW = step(mod(cPos.y * 23.4, 1.0), 0.5);

    vec4 color = BG_COLOR * bgW + mix(STRIPE_1_COLOR, STRIPE_2_COLOR, stripeW) * beeW;
    return color;
  }

  void main() {
    if (uPattern == 1) {
      gl_FragColor = rollingHills();
    } else if (uPattern == 2) {
      gl_FragColor = warpedPlane();
    } else if (uPattern == 3) {
      gl_FragColor = rothko();
    } else if (uPattern == 4) {
      gl_FragColor = woodShavings();
    } else if (uPattern == 5) {
      gl_FragColor = wood();
    } else if (uPattern == 6) {
      gl_FragColor = blueMineral();
    } else if (uPattern == 7) {
      gl_FragColor = ripple();
    } else if (uPattern == 8) {
      gl_FragColor = bee();
    } else {
      gl_FragColor = vec4(1.0);
    }
  }
`;

const VERTEX_SHADER_SOURCE = `
  precision mediump float;

  attribute vec4 aVertexPosition;

  void main() {
    gl_Position = aVertexPosition;
  }
`;

let buffers;
let programInfo;

function initBuffers(gl) {
  const positions = [
    -1.0, -1.0,
     1.0, -1.0,
     1.0, 1.0,
    -1.0, 1.0,
  ];
  const positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

  const indices = [
    0, 1, 2,
    0, 2, 3,
  ];
  const indexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(
    gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

  buffers = {
    indices: indexBuffer,
    position: positionBuffer,
  };
}

function initShaderProgram(gl) {
  programInfo =
    createShaderProgram(gl, VERTEX_SHADER_SOURCE, FRAGMENT_SHADER_SOURCE);
  programInfo.uniformLocations.pattern =
    gl.getUniformLocation(programInfo.program, 'uPattern');
}

export function init(gl) {
  initBuffers(gl);
  initShaderProgram(gl);
}

export function getProgramInfo() {
  return programInfo;
}

export function advanceState() {}

export function getSpec() {
  return {
    name: 'BoS: Noise',
    params: [
      {
        name: 'pattern',
        readableName: 'Pattern',
        type: 'enum',
        enumValues: Object.keys(PATTERN_TYPE),
      }
    ]
  };
}

export function setParameters(gl, params) {
  gl.useProgram(programInfo.program);
  gl.uniform1i(
    programInfo.uniformLocations.pattern, PATTERN_TYPE[params.pattern]);
}

export function draw(gl, delta) {
  gl.useProgram(programInfo.program);

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.clearDepth(1.0);
  gl.enable(gl.DEPTH_TEST);
  gl.depthFunc(gl.LEQUAL);

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
  gl.vertexAttribPointer(
    programInfo.attribLocations.vertexPosition, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

  gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
}



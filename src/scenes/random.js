// Chapter 10 from the Book of Shaders (at https://thebookofshaders.com/10/).

import {createShaderProgram} from '../glUtils.js';

const PATTERN_TYPE = {
  'RANDOM_NOISE': 1,
  'DIRT_PATHS': 2,
  'BARCODES': 3,
  'STAR_FLOW': 4,
};

const FRAGMENT_SHADER_SOURCE = `
  precision mediump float;

  const float PI = 3.14159265359;
  const float PI_SEED = 31415.9265359;

  uniform vec2 uMouseRel;
  uniform vec2 uResolution;
  uniform float uTimeMillis;

  uniform int uPattern;

  float random(vec2 pos, float seed) {
    return fract(sin(dot(pos, vec2(1234.56789, 9876.54321))) * seed);
  }

  vec2 rotate90Cw(vec2 center, vec2 pos) {
    pos -= center;
    return vec2(pos.y, -pos.x) + center;
  }

  float circleMask(vec2 center, float radius, vec2 pos) {
    return step(length(pos - center), radius);
  }

  float truncate(float x, float f) {
    return floor(x * f) / f;
  }

  float between(float x, float low, float high) {
    return step(low, x) * step(x, high);
  }

  vec4 randomNoise() {
    vec2 pos = gl_FragCoord.xy / uResolution;
    float s = PI_SEED + uTimeMillis / 100.0;
    float r = random(pos, s);
    return vec4(vec3(r), 1.0);
  }

  vec4 dirtPaths() {
    const vec4 BG_COLOR = vec4(0.357, 0.631, 0.388, 1.0);
    const vec4 FG_COLOR = vec4(1.0, 0.937, 0.839, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;

    float timeStep = uTimeMillis / 8000.0;
    vec2 scalePos = vec2(pos.x + timeStep * 2.0, pos.y + sin(timeStep * PI)) * 30.0;
    vec2 outTilePos = floor(scalePos);
    vec2 inTilePos = fract(scalePos);

    if (random(outTilePos, PI_SEED) > 0.5) {
      inTilePos = rotate90Cw(vec2(0.5), inTilePos);
    }

    float w1 = circleMask(vec2(0.0), 0.6, inTilePos) - circleMask(vec2(0.0), 0.4, inTilePos);
    float w2 = circleMask(vec2(1.0), 0.6, inTilePos) - circleMask(vec2(1.0), 0.4, inTilePos);
    float w = w1 + w2;

    return mix(BG_COLOR, FG_COLOR, w);
  }

  vec4 barcodes() {
    vec2 pos = gl_FragCoord.xy / uResolution;
    float t = uTimeMillis / 1000.0;
    float tStep = floor(t);
    float tPart = fract(t);

    float row = floor(pos.y * 7.0);
    float dir = mod(row, 2.0) * 2.0 - 1.0;

    float tX = mod(abs(tStep * tStep), 987.654);
    float density = mod(tX * 123.456, 0.25) + 0.25;
    float width = mod(tX * 234.567, 200.0) + 100.0;
    float speed = mod(tX * 345.678, 0.03) + 0.02;
    float sign = step(mod(tX * 456.789, 2.0), 1.0) * 2.0 - 1.0;

    float value = truncate(pos.x, width);
    value = value + tPart * dir * sign * speed + row;
    value = truncate(value, width);

    float w = step(density, random(vec2(value), PI_SEED));
    return vec4(vec3(w), 1.0);
  }

  vec4 starFlow() {
    const vec4 COLOR1 = vec4(0.996, 0.925, 0.510, 1.0);
    const vec4 COLOR2 = vec4(0.878, 0.878, 0.914, 1.0);
    const vec4 COLOR3 = vec4(1.000, 0.839, 0.871, 1.0);
    const vec4 BG_COLOR = vec4(0.059, 0.075, 0.129, 1.0);

    const float COLOR1_CUTOFF = 0.01;
    const float COLOR2_CUTOFF = 0.03;
    const float COLOR3_CUTOFF = 0.04;
    const float TRAIL_FACTOR = 2.0;
    const float ROW_COUNT = 200.0;
    const float DENSITY = 200.0;

    vec2 pos = gl_FragCoord.xy / uResolution;
    float t = uTimeMillis / 1000.0;

    float row = floor(pos.y * ROW_COUNT) + 1.0;
    float rX = mod(row * row * PI, 4.56789) + 1.0;

    float value = truncate(pos.x, DENSITY);
    value = value / (rX * TRAIL_FACTOR) + t * rX / 40.0 + rX;
    value = mod(truncate(value, DENSITY), 100.0);
    float x = random(vec2(value), PI_SEED + t / 1000.0);

    float limit1 = 0.0;
    float limit2 = uMouseRel.x * COLOR1_CUTOFF;
    float limit3 = uMouseRel.x * COLOR2_CUTOFF;
    float limit4 = uMouseRel.x * COLOR3_CUTOFF;

    float w1 = between(x, limit1, limit2);
    float w2 = between(x, limit2, limit3);
    float w3 = between(x, limit3, limit4);
    float wB = 1.0 - w1 - w2 - w3;

    return COLOR1 * w1 + COLOR2 * w2 + COLOR3 * w3 + BG_COLOR * wB;
  }

  void main() {
    if (uPattern == 1) {
      gl_FragColor = randomNoise();
    } else if (uPattern == 2) {
      gl_FragColor = dirtPaths();
    } else if (uPattern == 3) {
      gl_FragColor = barcodes();
    } else if (uPattern == 4) {
      gl_FragColor = starFlow();
    } else {
      gl_FragColor = vec4(1.0);
    }
  }
`;

const VERTEX_SHADER_SOURCE = `
  precision mediump float;

  attribute vec4 aVertexPosition;

  void main() {
    gl_Position = aVertexPosition;
  }
`;

let buffers;
let programInfo;

function initBuffers(gl) {
  const positions = [
    -1.0, -1.0,
     1.0, -1.0,
     1.0, 1.0,
    -1.0, 1.0,
  ];
  const positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

  const indices = [
    0, 1, 2,
    0, 2, 3,
  ];
  const indexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(
    gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

  buffers = {
    indices: indexBuffer,
    position: positionBuffer,
  };
}

function initShaderProgram(gl) {
  programInfo =
    createShaderProgram(gl, VERTEX_SHADER_SOURCE, FRAGMENT_SHADER_SOURCE);
  programInfo.uniformLocations.pattern =
    gl.getUniformLocation(programInfo.program, 'uPattern');
}

export function init(gl) {
  initBuffers(gl);
  initShaderProgram(gl);
}

export function getProgramInfo() {
  return programInfo;
}

export function advanceState() {}

export function getSpec() {
  return {
    name: 'BoS: Random',
    params: [
      {
        name: 'pattern',
        readableName: 'Pattern',
        type: 'enum',
        enumValues: Object.keys(PATTERN_TYPE),
      }
    ]
  };
}

export function setParameters(gl, params) {
  gl.useProgram(programInfo.program);
  gl.uniform1i(
    programInfo.uniformLocations.pattern, PATTERN_TYPE[params.pattern]);
}

export function draw(gl, delta) {
  gl.useProgram(programInfo.program);

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.clearDepth(1.0);
  gl.enable(gl.DEPTH_TEST);
  gl.depthFunc(gl.LEQUAL);

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
  gl.vertexAttribPointer(
    programInfo.attribLocations.vertexPosition, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

  gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
}


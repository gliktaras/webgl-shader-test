// Chapter 9 from the Book of Shaders (at https://thebookofshaders.com/09/).

import {createShaderProgram} from '../glUtils.js';

const PATTERN_TYPE = {
  'COLORS': 1,
  'TIC_TAC_TOE': 2,
  'CHECKERS': 3,
  'OCTAGON_TILE': 4,
  'TARTAN': 5,
  'BRICKS': 6,
  'POLKA_DOTS': 7,
  'TRUCHET': 8,
};

const FRAGMENT_SHADER_SOURCE = `
  precision mediump float;

  const float PI = 3.14159265359;
  const float TWO_PI = 6.28318530718;

  uniform vec2 uMouseRel;
  uniform vec2 uResolution;
  uniform float uTimeMillis;

  uniform int uPattern;

  vec2 rotate(float theta, vec2 center, vec2 pos) {
    mat2 TRANSFORM = mat2(cos(theta), sin(theta), -sin(theta), cos(theta));
    return TRANSFORM * (pos - center) + center;
  }

  float circleMask(vec2 center, float radius, vec2 pos) {
    return step(length(pos - center), radius);
  }

  float smoothCircleMask(vec2 center, float radius, float factor, vec2 pos) {
    return smoothstep(radius * factor, radius / factor, length(pos - center));
  }

  float rectMask(vec2 bottomLeft, vec2 topRight, vec2 pos) {
    vec2 mask = step(bottomLeft, pos) * step(pos, topRight);
    return mask.x * mask.y;
  }

  float cautionStripeMask(int density, float offset, vec2 pos) {
    pos = fract(pos * float(density) + vec2(offset, 0.0));
    float x = pos.x + 1.0 - pos.y;
    return step(x, 0.5) + step(1.0, x) * step(x, 1.5);
  }

  vec4 colors() {
    vec2 tiling = vec2(5.0 - uMouseRel.x * 2.0, 3.0 + uMouseRel.y * 2.0);
    vec2 pos = gl_FragCoord.xy / uResolution;
    pos = fract(pos * tiling);
    return vec4(pos.x, pos.y, 1.0, 1.0);
  }

  vec4 ticTacToe() {
    const vec4 BG_COLOR = vec4(0.996, 0.996, 0.996, 1.0);
    const vec4 GRID_COLOR = vec4(0.220, 0.071, 0.075, 1.0);
    const vec4 CROSS_COLOR = vec4(0.894, 0.0, 0.184, 1.0);
    const vec4 CIRCLE_COLOR = vec4(0.004, 0.400, 0.498, 1.0);

    const float THIRD1 = 1.0 / 3.0;
    const float THIRD2 = 2.0 / 3.0;

    vec2 pos = gl_FragCoord.xy / uResolution;
    vec2 tilePos = fract(pos * 3.0);
    vec2 tileCoord = floor(pos * 3.0);
    int tile = int(tileCoord.y * 3.0 + tileCoord.x);

    float circleW = 0.0;
    if (tile == 0 || tile == 3 || tile == 4) {
      circleW = circleMask(vec2(0.5), 0.35, tilePos) - circleMask(vec2(0.5), 0.25, tilePos);
    }
    float crossW = 0.0;
    if (tile == 2 || tile == 6 || tile == 8) {
      vec2 crossPos = rotate(PI / 4.0, vec2(0.5), tilePos);
      crossW = min(
          rectMask(vec2(0.45, 0.1), vec2(0.55, 0.9), crossPos) +
          rectMask(vec2(0.1, 0.45), vec2(0.9, 0.55), crossPos), 1.0);
    }
    float gridW = min(
        rectMask(vec2(THIRD1 - 0.01, 0.03), vec2(THIRD1 + 0.01, 0.97), pos) +
        rectMask(vec2(THIRD2 - 0.01, 0.03), vec2(THIRD2 + 0.01, 0.97), pos) +
        rectMask(vec2(0.03, THIRD1 - 0.01), vec2(0.97, THIRD1 + 0.01), pos) +
        rectMask(vec2(0.03, THIRD2 - 0.01), vec2(0.97, THIRD2 + 0.01), pos),
        1.0);
    float bgW = 1.0 - min(circleW + crossW + gridW, 1.0);

    return BG_COLOR * bgW + GRID_COLOR * gridW + CROSS_COLOR * crossW + CIRCLE_COLOR * circleW;
  }

  vec4 checkers() {
    const vec4 COLOR1 = vec4(0.302, 0.298, 0.349, 1.0);
    const vec4 COLOR2 = vec4(0.969, 0.965, 0.941, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    vec2 tilePos = fract(pos * 8.0);

    float rCos = cos(uTimeMillis / 1000.0);
    float rAngle = rCos * 0.25 * PI;
    vec2 rPos = rotate(rAngle, vec2(0.5), tilePos);
    vec2 rCorner = rotate(rAngle, vec2(0.0), vec2(0.5));
    float rSize = 0.25 / max(abs(rCorner.x), abs(rCorner.y));

    float weight = rectMask(vec2(0.5 - rSize), vec2(0.5 + rSize), rPos);
    weight = 0.5 + rCos * 0.5 * (weight * 2.0 - 1.0);

    return mix(COLOR1, COLOR2, weight);
  }

  vec4 octagonTile() {
    const vec4 OCTAGON_COLOR = vec4(0.929, 0.929, 0.937, 1.0);
    const vec4 RECT_COLOR = vec4(0.890, 0.906, 0.624, 1.0);
    const vec4 LINE_COLOR = vec4(0.420, 0.383, 0.357, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    pos = rotate(uTimeMillis / 10000.0, vec2(0.5), pos);
    vec2 tilePos = fract(pos * 8.0);

    vec2 diamondPos = rotate(PI / 4.0, vec2(0.5), tilePos);
    float lineW =
        rectMask(vec2(0.3), vec2(0.7), diamondPos) +
        rectMask(vec2(0.475, 0.0), vec2(0.525, 1.0), tilePos) +
        rectMask(vec2(0.0, 0.475), vec2(1.0, 0.525), tilePos);
    float rectW = rectMask(vec2(0.35), vec2(0.65), diamondPos);
    lineW = max(min(lineW, 1.0) - rectW, 0.0);
    float octagonW = max(1.0 - rectW - lineW, 0.0);

    return OCTAGON_COLOR * octagonW + RECT_COLOR * rectW + LINE_COLOR * lineW;
  }

  vec4 tartan() {
    const vec4 BASE_COLOR = vec4(0.737, 0.737, 0.737, 1.0);
    const vec4 STRIPE_1_COLOR = vec4(0.949, 0.949, 0.949, 1.0);
    const vec4 STRIPE_2_COLOR = vec4(0.043, 0.043, 0.043, 1.0);
    const vec4 STRIPE_3_COLOR = vec4(0.886, 0.153, 0.176, 1.0);
    const int DENSITY = 30;

    vec2 pos = gl_FragCoord.xy / uResolution;
    vec2 tilePos = fract(rotate(uTimeMillis / 10000.0, vec2(0.3), pos) * 3.0);
    vec2 quadPos = abs(tilePos - vec2(0.5));

    float hDiagMask = cautionStripeMask(DENSITY, 0.0, tilePos);
    float vDiagMask = 1.0 - hDiagMask;

    float h1Area = rectMask(vec2(0.0, 0.05), vec2(0.5, 0.15), quadPos);
    float v1Area = rectMask(vec2(0.05, 0.0), vec2(0.15, 0.5), quadPos);
    float s1W = h1Area * hDiagMask + v1Area * vDiagMask;

    float h2Area = rectMask(vec2(0.0, 0.0), vec2(0.5, 0.25), quadPos) - h1Area;
    float v2Area = rectMask(vec2(0.0, 0.0), vec2(0.25, 0.5), quadPos) - v1Area;
    float s2W = h2Area * hDiagMask + v2Area * vDiagMask;

    float h3Area = rectMask(vec2(0.0, 0.4825), vec2(0.5, 0.5), quadPos);
    float v3Area = rectMask(vec2(0.4825, 0.0), vec2(0.5, 0.5), quadPos);
    float s3W = h3Area * hDiagMask + v3Area * vDiagMask;

    float baseW = max(1.0 - s1W - s2W - s3W, 0.0);

    return BASE_COLOR * baseW + STRIPE_1_COLOR * s1W + STRIPE_2_COLOR * s2W + STRIPE_3_COLOR * s3W;
  }

  vec4 bricks() {
    const vec4 BRICK_COLOR = vec4(0.643, 0.239, 0.204, 1.0);
    const vec4 FILL_COLOR = vec4(0.941, 0.933, 0.933, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    vec2 tilePos = pos * 10.0 / vec2(2.15, 0.65);
    float isOdd = step(mod(tilePos.y, 2.0), 1.0);
    float dirSign = isOdd * 2.0 - 1.0;
    float tileXOffset = 0.5 * isOdd + dirSign * uTimeMillis / 2000.0;
    tilePos = fract(tilePos + vec2(tileXOffset, 0.0));

    float w = rectMask(vec2(0.0233, 0.05), vec2(0.9767, 0.95), tilePos);
    return mix(FILL_COLOR, BRICK_COLOR, w);
  }

  vec4 polkaDots() {
    const vec4 DOT_COLOR = vec4(1.0);
    const vec4 BACK_COLOR = vec4(0.851, 0.424, 0.639, 1.0);
    vec2 pos = gl_FragCoord.xy / uResolution;

    float timeStep = uTimeMillis / 1000.0;
    float moveAxis = step(mod(timeStep, 2.0), 1.0);  // 1.0 = x, 0.0 = y.
    float moveOffset = fract(timeStep);

    vec2 tilePos = pos * 20.0;
    float isOddCol = step(mod(tilePos.x, 2.0), 1.0) * 2.0 - 1.0;
    float isOddRow = step(mod(tilePos.y, 2.0), 1.0) * 2.0 - 1.0;
    float xOffset = moveAxis * isOddRow * moveOffset;
    float yOffset = (1.0 - moveAxis) * isOddCol * moveOffset;
    tilePos = fract(tilePos + vec2(xOffset, yOffset));

    float w = smoothCircleMask(vec2(0.5), 0.3, 0.9, tilePos);
    return mix(BACK_COLOR, DOT_COLOR, w);
  }

  vec4 truchet() {
    const float DENSITY = 20.0;
    const vec4 POSITIVE_1_COLOR = vec4(1.0, 0.769, 0.373, 1.0);
    const vec4 POSITIVE_2_COLOR = vec4(1.0, 0.941, 0.737, 1.0);
    const vec4 NEGATIVE_1_COLOR = vec4(0.012, 0.267, 0.533, 1.0);
    const vec4 NEGATIVE_2_COLOR = vec4(0.090, 0.561, 0.839, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    float timeStep = floor(uTimeMillis / 1000.0);

    vec2 tilePos = abs(pos - vec2(0.5)) * DENSITY;
    float index = floor(mod(tilePos.x, DENSITY)) + timeStep * floor(mod(tilePos.y, DENSITY)) + timeStep;
    tilePos = fract(tilePos);

    float quad = floor(mod(index * timeStep * PI, 8.0));
    vec2 cellPos = rotate(PI * quad / 4.0, vec2(0.5), tilePos);
    float w = step(cellPos.x, cellPos.y);

    vec4 positive = mix(POSITIVE_1_COLOR, POSITIVE_2_COLOR, pos.y);
    vec4 negative = mix(NEGATIVE_1_COLOR, NEGATIVE_2_COLOR, pos.y);
    return mix(positive, negative, w);
  }

  void main() {
    if (uPattern == 1) {
      gl_FragColor = colors();
    } else if (uPattern == 2) {
      gl_FragColor = ticTacToe();
    } else if (uPattern == 3) {
      gl_FragColor = checkers();
    } else if (uPattern == 4) {
      gl_FragColor = octagonTile();
    } else if (uPattern == 5) {
      gl_FragColor = tartan();
    } else if (uPattern == 6) {
      gl_FragColor = bricks();
    } else if (uPattern == 7) {
      gl_FragColor = polkaDots();
    } else if (uPattern == 8) {
      gl_FragColor = truchet();
    } else {
      gl_FragColor = vec4(1.0);
    }
  }
`;

const VERTEX_SHADER_SOURCE = `
  precision mediump float;

  attribute vec4 aVertexPosition;

  void main() {
    gl_Position = aVertexPosition;
  }
`;

let buffers;
let programInfo;

function initBuffers(gl) {
  const positions = [
    -1.0, -1.0,
     1.0, -1.0,
     1.0, 1.0,
    -1.0, 1.0,
  ];
  const positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

  const indices = [
    0, 1, 2,
    0, 2, 3,
  ];
  const indexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(
    gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

  buffers = {
    indices: indexBuffer,
    position: positionBuffer,
  };
}

function initShaderProgram(gl) {
  programInfo =
    createShaderProgram(gl, VERTEX_SHADER_SOURCE, FRAGMENT_SHADER_SOURCE);
  programInfo.uniformLocations.pattern =
    gl.getUniformLocation(programInfo.program, 'uPattern');
}

export function init(gl) {
  initBuffers(gl);
  initShaderProgram(gl);
}

export function getProgramInfo() {
  return programInfo;
}

export function advanceState() {}

export function getSpec() {
  return {
    name: 'BoS: Patterns',
    params: [
      {
        name: 'pattern',
        readableName: 'Pattern',
        type: 'enum',
        enumValues: Object.keys(PATTERN_TYPE),
      }
    ]
  };
}

export function setParameters(gl, params) {
  gl.useProgram(programInfo.program);
  gl.uniform1i(
    programInfo.uniformLocations.pattern, PATTERN_TYPE[params.pattern]);
}

export function draw(gl, delta) {
  gl.useProgram(programInfo.program);

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.clearDepth(1.0);
  gl.enable(gl.DEPTH_TEST);
  gl.depthFunc(gl.LEQUAL);

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
  gl.vertexAttribPointer(
    programInfo.attribLocations.vertexPosition, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

  gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
}


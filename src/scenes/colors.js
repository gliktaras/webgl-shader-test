// Chapter 6 from the Book of Shaders (at https://thebookofshaders.com/06/).

import {createShaderProgram} from '../glUtils.js';

const COLORING_TYPE = {
  'BEACH': 1,
  'HEADACHE': 2,
  'GETTING_IT': 3,
  'DOZING': 4,
  'SKY': 5,
  'RAINBOW_RGB': 6,
  'COLOR_WHEEL_STRIPES': 7,
  'HSV_PLANE': 8,
  'HSV_DISC': 9,
  'RYB_DISC': 10,
  'IOC_1': 11,
  'IOC_2': 12,
  'IOC_3': 13,
  'IOC_4': 14,
  'IOC_5': 15,
  'IOC_6': 16,
  'IOC_7': 17,
};

const FRAGMENT_SHADER_SOURCE = `
  precision mediump float;

  const float PI = 3.14159265359;
  const float TWO_PI = 6.28318530718;

  uniform vec2 uMouseRel;
  uniform vec2 uResolution;
  uniform float uTimeMillis;

  uniform int uColoring;
  uniform sampler2D uRybSamplerTop;
  uniform sampler2D uRybSamplerBottom;

  vec3 rgb2hsb(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
  }

  vec3 hsb2rgb(vec3 c) {
    vec3 rgb = clamp(abs(mod(c.x * 6.0 + vec3(0.0, 4.0, 2.0), 6.0) - 3.0) - 1.0, 0.0, 1.0);
    rgb = rgb * rgb * (3.0 - 2.0 * rgb);
    return c.z * mix(vec3(1.0), rgb, c.y);
  }

  vec3 rgb2ryb(vec3 c) {
    vec4 colorTop = texture2D(uRybSamplerTop, c.rg);
    vec4 colorBottom = texture2D(uRybSamplerBottom, c.rg);
    return mix(colorBottom, colorTop, c.b).rgb;
  }

  vec4 beach() {
    float w = sin(uTimeMillis / 1000.0) / 2.0 + 0.5;
    return vec4(mix(vec3(0.2, 0.2, 0.9), vec3(1.0, 0.9, 0.2), w), 1.0);
  }

  vec4 headache() {
    float t = mod(uTimeMillis / 1000.0, 15.0);
    float w = abs(sin(t * PI)) * min(t, 10.0) / 10.0;
    return vec4(mix(vec3(0.0, 0.0, 0.0), vec3(1.0, 0.0, 0.0), w), 1.0);
  }

  vec4 gettingIt() {
    float t = fract(uTimeMillis / 10000.0);
    float a = 0.2;
    float b = 0.8;
    float k = 1.0 / (pow(a, a) * pow(b, b));
    float w = k * pow(t, a) * pow(1.0 - t, b);
    return vec4(mix(vec3(0.2, 0.1, 0.1), vec3(1.0, 1.0, 1.0), w), 1.0);
  }

  vec4 dozing() {
    float t = fract(uTimeMillis / 10000.0);
    float w = pow(t, 3.0);
    return vec4(mix(vec3(0.8, 0.8, 0.9), vec3(0.4, 0.4, 0.4), w), 1.0);
  }

  vec4 sky() {
    const vec3 NIGHT_SKY = vec3(0.05, 0.1, 0.4);
    const vec3 DAWN_RED_SKY = vec3(1.0, 0.7, 0.3);
    const vec3 DAWN_BLUE_SKY = vec3(0.9, 0.95, 1.0);
    const vec3 DAY_SKY = vec3(0.9, 0.95, 1.0);
    const vec3 DUSK_RED_SKY = vec3(1.0, 0.4, 0.1);
    const vec3 DUSK_BLUE_SKY = vec3(0.9, 0.95, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    float t = mod(uTimeMillis / 500.0, 18.0);
    vec3 color;

    if (t <= 6.0) {
      // Dawn.

      // Line transition from dark to blue sky.
      float bS = clamp(pos.y * 3.0 / (t * t), 0.0, PI / 2.0);
      float bW = 1.0 - sin(bS);
      color = mix(NIGHT_SKY, DAWN_BLUE_SKY, bW);

      // Line transition from red to blue sky.
      float rS = min(bS * 12.0, PI / 2.0);
      float rW = 1.0 - sin(rS);
      rW = mix(rW, 0.0, clamp(t / 5.0, 0.0, 1.0));
      color = mix(color, DAWN_RED_SKY, rW);

      // Screen transition to day sky.
      float dW = clamp((t - 3.0) / 3.0, 0.0, 1.0);
      color = mix(color, DAY_SKY, dW);
    } else if (t <= 9.0) {
      color = DAY_SKY;
    } else if (t <= 15.0) {
      float tS = t - 9.0;
      float tI = 6.0 - tS;
      // Dusk.

      // Line transition from dark to blue sky.
      float bS = clamp(pos.y * 3.0 / (tI * tI), 0.0, PI / 2.0);
      float bW = 1.0 - sin(bS);
      color = mix(NIGHT_SKY, DUSK_BLUE_SKY, bW);

      // Line transition from red to blue sky.
      float rS = min(bS * 12.0, PI / 2.0);
      float rW = 1.0 - sin(rS);
      rW = mix(rW, 0.0, clamp(tS / 6.0, 0.0, 1.0));
      color = mix(color, DUSK_RED_SKY, rW);

      // Screen transition to dusk sky.
      float dW = clamp(tS / 3.0, 0.0, 1.0);
      color = mix(DAY_SKY, color, dW);
    } else {
      color = NIGHT_SKY;
    }

    return vec4(color, 1.0);
  }

  vec4 rainbowRgb() {
    // Linear interpolation in RGB space is pretty ugly.
    const vec2 ANTISOLAR_POINT = vec2(0.5, -0.2);
    float rDist = length(vec2(uMouseRel.x, 1.0 - uMouseRel.y) - ANTISOLAR_POINT);
    float pDist = length(gl_FragCoord.xy / uResolution - ANTISOLAR_POINT);

    float s = clamp((rDist - pDist) * 10.0 + 0.5, 0.0, 1.0);
    vec3 color;
    if (s <= 0.0676) {
      color = mix(
        vec3(0.0000, 0.0000, 0.0000),
        vec3(0.2470, 0.0588, 0.5137),
        smoothstep(0.0, 0.0676, s));
    } else if (s <= 0.1098) {
      color = mix(
        vec3(0.2470, 0.0588, 0.5137),
        vec3(0.0980, 0.1019, 0.9411),
        smoothstep(0.0676, 0.1098, s));
    } else if (s <= 0.2247) {
      color = mix(
        vec3(0.0980, 0.1019, 0.9411),
        vec3(0.0000, 0.7098, 0.7529),
        smoothstep(0.1098, 0.2247, s));
    } else if (s <= 0.3784) {
      color = mix(
        vec3(0.0000, 0.7098, 0.7529),
        vec3(0.0117, 0.9333, 0.0901),
        smoothstep(0.2247, 0.3784, s));
    } else if (s <= 0.5321) {
      color = mix(
        vec3(0.0117, 0.9333, 0.0901),
        vec3(0.9058, 0.7725, 0.0039),
        smoothstep(0.3784, 0.5321, s));
    } else if (s <= 0.8243) {
      color = mix(
        vec3(0.9058, 0.7725, 0.0039),
        vec3(0.9921, 0.0941, 0.0039),
        smoothstep(0.5321, 0.8243, s));
    } else if (s <= 0.9324) {
      color = mix(
        vec3(0.9921, 0.0941, 0.0039),
        vec3(0.5098, 0.0392, 0.0352),
        smoothstep(0.8243, 0.9324, s));
    } else {
      color = mix(
        vec3(0.5098, 0.0392, 0.0352),
        vec3(0.0, 0.0, 0.0),
        smoothstep(0.9324, 1.0, s));
    }
    return vec4(color, 1.0);
  }

  vec4 colorWheelStripes() {
    vec2 pos = gl_FragCoord.xy / uResolution;
    float tF = pos.x + pos.y / 3.0 + uTimeMillis / 10000.0;
    int t = int(mod(tF * 10.0, 6.0));

    vec3 color;
    if (t == 0) {
      color = vec3(1.0, 0.9, 0.0);
    } else if (t == 1) {
      color = vec3(1.0, 0.75, 0.0);
    } else if (t == 2) {
      color = vec3(1.0, 0.1, 0.0);
    } else if (t == 3) {
      color = vec3(0.75, 0.25, 0.75);
    } else if (t == 4) {
      color = vec3(0.0, 0.6, 1.0);
    } else {
      color = vec3(0.0, 0.8, 0.2);
    }
    return vec4(color, 1.0);
  }

  vec4 hsvPlane() {
    vec2 pos = gl_FragCoord.xy / uResolution;
    return vec4(hsb2rgb(vec3(pos.x, 1.0, pos.y)), 1.0);
  }

  vec4 hsvDisc() {
    vec2 pos = gl_FragCoord.xy / uResolution;
    vec2 toCenter = pos - vec2(0.5);
    float angle = atan(toCenter.y, toCenter.x);
    float radius = length(toCenter) * 2.0;

    float hue = angle / TWO_PI + 0.5;
    hue = mod(hue + uTimeMillis / 10000.0, 1.0);
    vec4 color = vec4(hsb2rgb(vec3(hue, radius, 1.0)), 1.0);
    if (radius > 1.0) {
      color = vec4(1.0);
    }
    return color;
  }

  vec4 rybDisc() {
    vec2 pos = gl_FragCoord.xy / uResolution;
    vec2 toCenter = pos - vec2(0.5);
    float angle = atan(toCenter.y, toCenter.x);
    float radius = length(toCenter) * 2.0;

    float hue = angle / TWO_PI + 0.5;
    hue = mod(hue + uTimeMillis / 10000.0, 1.0);
    float sat = clamp(radius, 0.0, 1.0);
    vec4 color = vec4(rgb2ryb(hsb2rgb(vec3(hue, sat, 1.0))), 1.0);
    if (radius > 1.0) {
      color = vec4(1.0);
    }

    return color;
  }

  vec4 ioc1() {
    const vec4 COLOR1 = vec4(0.980, 0.576, 0.114, 1.0);
    const vec4 COLOR2 = vec4(0.651, 0.918, 0.984, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    float weight = step(pos.x, 0.5);
    if (abs(0.25 - mod(pos.x, 0.5)) < 0.05 && abs(0.5 - pos.y) < 0.16667) {
      weight = 0.5;
    }
    return mix(COLOR1, COLOR2, weight);
  }

  vec4 ioc2() {
    const vec4 COLOR1 = vec4(0.627, 0.525, 0.776, 1.0);
    const vec4 COLOR2 = vec4(0.318, 0.169, 0.365, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    float weight = step(pos.x, 0.5);
    if (abs(0.25 - mod(pos.x, 0.5)) < 0.05 && abs(0.5 - pos.y) < 0.16667) {
      weight = 0.5;
    }
    return mix(COLOR1, COLOR2, weight);
  }

  vec4 ioc3() {
    const vec4 COLOR1 = vec4(0.420, 0.733, 0.780, 1.0);
    const vec4 COLOR2 = vec4(0.067, 0.067, 0.290, 1.0);
    const vec4 COLOR3 = vec4(0.867, 0.843, 0.161, 1.0);
    const vec4 COLOR4 = vec4(0.980, 0.604, 0.086, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    vec4 color;

    float weight = step(pos.y, 0.5);
    if (abs(0.5 - pos.x) < 0.1 && abs(0.5 - pos.y) < 0.4) {
      weight = 0.5;
    }
    if (abs(0.5 - pos.y) < 0.225) {
      color = pos.y < 0.5 ? COLOR2 : COLOR3;
    } else {
      color = mix(COLOR1, COLOR4, weight);
    }
    return color;
  }

  vec4 ioc4() {
    const vec4 COLOR1 = vec4(0.902, 0.945, 0.961, 1.0);
    const vec4 COLOR2 = vec4(0.294, 0.294, 0.294, 1.0);
    vec2 pos = gl_FragCoord.xy / uResolution;

    float weight = 1.0 - pos.y;
    if (abs(0.5 - pos.x) < 0.1 && abs(0.5 - pos.y) < 0.4) {
      weight = 0.5;
    }
    return mix(COLOR1, COLOR2, weight);
  }

  vec4 ioc5() {
    const vec4 COLOR1 = vec4(0.933, 0.976, 0.996, 1.0);
    const vec4 COLOR2 = vec4(0.039, 0.039, 0.039, 1.0);
    vec2 pos = gl_FragCoord.xy / uResolution;

    float weight = 1.0 - pos.y;
    if (abs(0.166667 - mod(pos.x, 0.333333)) < 0.066667
        && abs(0.5 - pos.y) < 0.4) {
      weight = 0.5;
    }
    return mix(COLOR1, COLOR2, weight);
  }

  vec4 ioc6() {
    const vec4 COLOR1 = vec4(0.498, 0.365, 0.565, 1.0);
    const vec4 COLOR2 = vec4(0.325, 0.278, 0.208, 1.0);
    const vec4 COLOR3 = vec4(0.514, 0.435, 0.416, 1.0);
    vec2 pos = gl_FragCoord.xy / uResolution;

    vec4 color = mix(COLOR1, COLOR2, step(pos.y, 0.5));
    if (abs(0.5 - pos.x) < 0.05 && abs(0.25 - mod(pos.y, 0.5)) < 0.05) {
      color = COLOR3;
    }
    return color;
  }

  vec4 ioc7() {
    const vec4 COLOR1 = vec4(0.980, 0.973, 0.894, 1.0);
    const vec4 COLOR2 = vec4(0.035, 0.722, 0.788, 1.0);
    const vec4 COLOR3 = vec4(0.878, 0.792, 0.580, 1.0);
    vec2 pos = gl_FragCoord.xy / uResolution;

    vec4 color = mix(COLOR1, COLOR2, step(pos.y, 0.5));
    if (abs(0.5 - pos.x) < 0.1 && abs(0.25 - mod(pos.y, 0.5)) < 0.05) {
      color = COLOR3;
    }
    return color;
  }

  void main() {
    if (uColoring == 1) {
      gl_FragColor = beach();
    } else if (uColoring == 2) {
      gl_FragColor = headache();
    } else if (uColoring == 3) {
      gl_FragColor = gettingIt();
    } else if (uColoring == 4) {
      gl_FragColor = dozing();
    } else if (uColoring == 5) {
      gl_FragColor = sky();
    } else if (uColoring == 6) {
      gl_FragColor = rainbowRgb();
    } else if (uColoring == 7) {
      gl_FragColor = colorWheelStripes();
    } else if (uColoring == 8) {
      gl_FragColor = hsvPlane();
    } else if (uColoring == 9) {
      gl_FragColor = hsvDisc();
    } else if (uColoring == 10) {
      gl_FragColor = rybDisc();
    } else if (uColoring == 11) {
      gl_FragColor = ioc1();
    } else if (uColoring == 12) {
      gl_FragColor = ioc2();
    } else if (uColoring == 13) {
      gl_FragColor = ioc3();
    } else if (uColoring == 14) {
      gl_FragColor = ioc4();
    } else if (uColoring == 15) {
      gl_FragColor = ioc5();
    } else if (uColoring == 16) {
      gl_FragColor = ioc6();
    } else if (uColoring == 17) {
      gl_FragColor = ioc7();
    } else {
      gl_FragColor = vec4(1.0);
    }
  }
`;

const VERTEX_SHADER_SOURCE = `
  precision mediump float;

  attribute vec4 aVertexPosition;

  void main() {
    gl_Position = aVertexPosition;
  }
`;

let buffers;
let textures;
let programInfo;

function initBuffers(gl) {
  const positions = [
    -1.0, -1.0,
     1.0, -1.0,
     1.0, 1.0,
    -1.0, 1.0,
  ];
  const positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

  const indices = [
    0, 1, 2,
    0, 2, 3,
  ];
  const indexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(
    gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

  buffers = {
    indices: indexBuffer,
    position: positionBuffer,
  };
}

function initTextures(gl) {
  const rybTop = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, rybTop);
  gl.texImage2D(
    gl.TEXTURE_2D, 0, gl.RGBA, 2, 2, 0, gl.RGBA, gl.UNSIGNED_BYTE,
    new Uint8Array([
      255, 255, 0, 255,  // Yellow
      255, 127, 0, 255,  // Orange
      0, 171, 51, 255,  // Green
      51, 24, 0, 255,  // Black
    ]));
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

  const rybBottom = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, rybBottom);
  gl.texImage2D(
    gl.TEXTURE_2D, 0, gl.RGBA, 2, 2, 0, gl.RGBA, gl.UNSIGNED_BYTE,
    new Uint8Array([
      255, 255, 255, 255,  // White
      255, 0, 0, 255,  // Red
      42, 95, 154, 255,  // Blue
      127, 0, 127, 255,  // Purple
    ]));
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

  textures = {
    rybTop: rybTop,
    rybBottom: rybBottom,
  };
}

function initShaderProgram(gl) {
  programInfo =
    createShaderProgram(gl, VERTEX_SHADER_SOURCE, FRAGMENT_SHADER_SOURCE);

  programInfo.uniformLocations.coloring =
    gl.getUniformLocation(programInfo.program, 'uColoring');
  programInfo.uniformLocations.rybSamplerTop =
    gl.getUniformLocation(programInfo.program, 'uRybSamplerTop');
  programInfo.uniformLocations.rybSamplerBottom =
    gl.getUniformLocation(programInfo.program, 'uRybSamplerBottom');
}

export function init(gl) {
  initBuffers(gl);
  initTextures(gl);
  initShaderProgram(gl);
}

export function getProgramInfo() {
  return programInfo;
}

export function advanceState() {}

export function getSpec() {
  return {
    name: 'BoS: Colors',
    params: [
      {
        name: 'coloring',
        readableName: 'Coloring',
        type: 'enum',
        enumValues: Object.keys(COLORING_TYPE),
      }
    ]
  };
}

export function setParameters(gl, params) {
  gl.useProgram(programInfo.program);
  gl.uniform1i(
    programInfo.uniformLocations.coloring, COLORING_TYPE[params.coloring]);
}

export function draw(gl, delta) {
  gl.useProgram(programInfo.program);

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.clearDepth(1.0);
  gl.enable(gl.DEPTH_TEST);
  gl.depthFunc(gl.LEQUAL);

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
  gl.vertexAttribPointer(
    programInfo.attribLocations.vertexPosition, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, textures.rybTop);
  gl.uniform1i(programInfo.uniformLocations.rybSamplerTop, 0);

  gl.activeTexture(gl.TEXTURE1);
  gl.bindTexture(gl.TEXTURE_2D, textures.rybBottom);
  gl.uniform1i(programInfo.uniformLocations.rybSamplerBottom, 1);

  gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
}


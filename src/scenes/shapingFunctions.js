// Chapter 5 from the Book of Shaders (at https://thebookofshaders.com/05/).

import {createShaderProgram} from '../glUtils.js';

const FUNCTION_TYPE = {
  'LINE': 1,
  'POWER': 2,
  'STEP': 3,
  'SMOOTHSTEP': 4,
  'SIN': 5,
  'MOD': 6,
  'DOUBLE_CUBIC_SEAT': 7,
  'QUADRATIC': 8,
  'DOUBLE_EXP': 9,
  'CIRCLE': 10,
  'DOUBLE_CIRCLE_SEAT': 11,
  'DOUBLE_ELLIPTIC_SEAT': 12,
  'QUADRATIC_BEZIER': 13,
  'IMPULSE': 14,
  'PARABOLA': 15,
  'POWER_CURVE': 16,
  'SINC': 17,
};

const FRAGMENT_SHADER_SOURCE = `
  precision mediump float;

  const float PI = 3.14159265359;

  const float LINE_WIDTH = 0.005;
  const vec3 LINE_COLOR = vec3(1.0, 1.0, 1.0);
  const float SLOPE_DELTA = 0.0001;

  const vec3 LOW_COLOR = vec3(0.2, 0.2, 0.5);
  const vec3 HIGH_COLOR = vec3(0.2, 1.0, 0.5);

  uniform vec2 uMouseRel;
  uniform vec2 uResolution;
  uniform float uTimeMillis;

  uniform int uFunction;

  float fLine(float x) {
    return x;
  }

  float fPower(float x) {
    float disp = (1.0 - uMouseRel.y - uMouseRel.x) / sqrt(2.0);
    float power = exp(-4.0 * disp);
    return pow(x, power);
  }

  float fStep(float x) {
    float xDisp = uMouseRel.x;
    float yDisp = (1.0 - abs(uMouseRel.y * 2.0 - 1.0)) / 2.0;
    yDisp = min(yDisp, 1.0 - yDisp);
    return clamp(step(xDisp, x), yDisp, 1.0 - yDisp);
  }

  float fSmoothStep(float x) {
    float xDisp = 0.5 - abs(uMouseRel.x - 0.5);
    float yDisp = (1.0 - abs(uMouseRel.y * 2.0 - 1.0)) / 2.0;
    yDisp = min(yDisp, 1.0 - yDisp);
    float step = smoothstep(xDisp, 1.0 - xDisp, x);
    return step * (1.0 - 2.0 * yDisp) + yDisp;
  }

  float fSin(float x) {
    float cycle = uTimeMillis / 1000.0;
    return sin(10.0 * PI * (x + cycle / 2.0)) * (1.0 - 2.0 * uMouseRel.y) / 2.0 + 0.5;
  }

  float fMod(float x) {
    float yDisp = (1.0 - uMouseRel.y);
    return mod(x, 0.2) * 5.0 * (1.0 - 2.0 * yDisp) + yDisp;
  }

  float fDoubleCubicSeat(float x) {
    float a = uMouseRel.x;
    float b = 1.0 - uMouseRel.y;
    if (x <= a) {
      return b - b * pow(1.0 - x / a, 3.0);
    } else {
      return b + (1.0 - b) * pow((x - a) / (1.0 - a), 3.0);
    }
  }

  float fPointedQuadratic(float x) {
    float a = uMouseRel.x;
    float b = 1.0 - uMouseRel.y;
    float y1 = (1.0 - b) / (1.0 - a) - b / a;
    float y2 = (a * a * y1 - b) / a;
    return y1 * x * x - y2 * x;
  }

  float fDoubleExp(float x) {
    float a = exp(5.0 * (0.5 - uMouseRel.y));
    if (x <= 0.5) {
      return pow(2.0 * x, a) / 2.0;
    } else {
      return 1.0 - pow(2.0 * (1.0 - x), a) / 2.0;
    }
  }

  float fCircle(float x) {
    if (uMouseRel.x >= (1.0 - uMouseRel.y)) {
      return 1.0 - sqrt(1.0 - x * x);
    } else {
      return sqrt(1.0 - (1.0 - x) * (1.0 - x));
    }
  }

  float fDoubleCircleSeat(float x) {
    float a = (uMouseRel.x + 1.0 - uMouseRel.y) / 2.0;
    if (x <= a) {
      return sqrt(a * a - (x - a) * (x - a));
    } else {
      return 1.0 - sqrt((1.0 - a) * (1.0 - a) - (x - a) * (x - a));
    }
  }

  float fDoubleEllipticSeat(float x) {
    float a = uMouseRel.x;
    float b = 1.0 - uMouseRel.y;
    if (x <= a) {
      return (b / a) * sqrt(a * a - (x - a) * (x - a));
    } else {
      return 1.0 - (1.0 - b) / (1.0 - a) * sqrt((1.0 - a) * (1.0 - a) - (x - a) * (x - a));
    }
  }

  float fQuadraticBezier(float x) {
    float a = uMouseRel.x;
    float b = 1.0 - uMouseRel.y;
    float t = (sqrt(a * a + (1.0 - 2.0 * a) * x) - a) / (1.0 - 2.0 * a);
    return (1.0 - 2.0 * b) * t * t + 2.0 * b * t;
  }

  float fImpulse(float x) {
    float w = 1.0 / uMouseRel.x * x;
    float h = 1.0 - uMouseRel.y;
    return w * exp(1.0 - w) * h;
  }

  float fParabola(float x) {
    float k = exp((0.25 - abs(uMouseRel.x - 0.5)) * 20.0);
    float h = 1.0 - uMouseRel.y;
    return pow(4.0 * x * (1.0 - x), k) * h;
  }

  float fPowerCurve(float x) {
    float a = uMouseRel.x;
    float b = 1.0 - uMouseRel.y;
    float k = pow(a + b, a + b) / (pow(a, a) * pow(b, b));
    return k * pow(x, a) * pow(1.0 - x, b);
  }

  float fSinc(float x) {
    float c = 20.0 * PI * x * (1.0 - uMouseRel.x);
    return (sin(c) / c) / 2.0 + 0.35;
  }

  float f(float x) {
    if (uFunction == 1) {
      return fLine(x);
    } else if (uFunction == 2) {
      return fPower(x);
    } else if (uFunction == 3) {
      return fStep(x);
    } else if (uFunction == 4) {
      return fSmoothStep(x);
    } else if (uFunction == 5) {
      return fSin(x);
    } else if (uFunction == 6) {
      return fMod(x);
    } else if (uFunction == 7) {
      return fDoubleCubicSeat(x);
    } else if (uFunction == 8) {
      return fPointedQuadratic(x);
    } else if (uFunction == 9) {
      return fDoubleExp(x);
    } else if (uFunction == 10) {
      return fCircle(x);
    } else if (uFunction == 11) {
      return fDoubleCircleSeat(x);
    } else if (uFunction == 12) {
      return fDoubleEllipticSeat(x);
    } else if (uFunction == 13) {
      return fQuadraticBezier(x);
    } else if (uFunction == 14) {
      return fImpulse(x);
    } else if (uFunction == 15) {
      return fParabola(x);
    } else if (uFunction == 16) {
      return fPowerCurve(x);
    } else if (uFunction == 17) {
      return fSinc(x);
    }
    return x;
  }

  float cubicPulse(float c, float w, float x) {
    x = abs(x - c);
    if (x > w) {
      return 0.0;
    }
    x /= w;
    return 1.0 - x * x * (3.0 - 2.0 * x);
  }

  float lineColorWeight(vec2 st, float y) {
    float x0 = max(st.x - SLOPE_DELTA, 0.0);
    float x1 = min(st.x + SLOPE_DELTA, 1.0);
    float y0 = f(x0);
    float y1 = f(x1);

    float xDelta = x1 - x0;
    float yDelta = y1 - y0;
    float h = sqrt(xDelta * xDelta + yDelta * yDelta);
    float w = h * LINE_WIDTH / xDelta;

    return cubicPulse(y, w, st.y);
  }

  void main() {
    vec2 st = gl_FragCoord.xy / uResolution.xy;
    float y = f(st.x);
    float w = lineColorWeight(st, y);

    vec3 bgColor = mix(LOW_COLOR, HIGH_COLOR, y);
    gl_FragColor = vec4(mix(bgColor, LINE_COLOR, w), 1.0);
  }
`;

const VERTEX_SHADER_SOURCE = `
  precision mediump float;

  attribute vec4 aVertexPosition;

  void main() {
    gl_Position = aVertexPosition;
  }
`;

let buffers;
let programInfo;

function initBuffers(gl) {
  const positions = [
    -1.0, -1.0,
     1.0, -1.0,
     1.0, 1.0,
    -1.0, 1.0,
  ];
  const positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

  const indices = [
    0, 1, 2,
    0, 2, 3,
  ];
  const indexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(
    gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

  buffers = {
    indices: indexBuffer,
    position: positionBuffer,
  };
}

function initShaderProgram(gl) {
  programInfo =
    createShaderProgram(gl, VERTEX_SHADER_SOURCE, FRAGMENT_SHADER_SOURCE);
  programInfo.uniformLocations.function =
    gl.getUniformLocation(programInfo.program, 'uFunction');
}

export function init(gl) {
  initBuffers(gl);
  initShaderProgram(gl);
}

export function getProgramInfo() {
  return programInfo;
}

export function advanceState() {}

export function getSpec() {
  return {
    name: 'BoS: Shaping functions',
    params: [
      {
        name: 'function',
        readableName: 'Function type',
        type: 'enum',
        enumValues: Object.keys(FUNCTION_TYPE),
      }
    ]
  };
}

export function setParameters(gl, params) {
  gl.useProgram(programInfo.program);
  gl.uniform1i(
    programInfo.uniformLocations.function, FUNCTION_TYPE[params.function]);
}

export function draw(gl, delta) {
  gl.useProgram(programInfo.program);

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.clearDepth(1.0);
  gl.enable(gl.DEPTH_TEST);
  gl.depthFunc(gl.LEQUAL);

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
  gl.vertexAttribPointer(
    programInfo.attribLocations.vertexPosition, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

  gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
}

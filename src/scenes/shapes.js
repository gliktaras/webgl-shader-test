// Chapter 7 from the Book of Shaders (at https://thebookofshaders.com/07/).

import {createShaderProgram} from '../glUtils.js';

const SHAPE_TYPE = {
  'SQUARE': 1,
  'MONDRIAN': 2,
  'CIRCLE': 3,
  'RIPPLE': 4,
  'DAISY': 5,
  'SPIN': 6,
  'REGULAR_POLYGONS': 7,
};

const FRAGMENT_SHADER_SOURCE = `
  precision mediump float;

  const float PI = 3.14159265359;
  const float TWO_PI = 6.28318530718;

  uniform vec2 uMouseRel;
  uniform vec2 uResolution;
  uniform float uTimeMillis;

  uniform int uShape;

  int rectMask(vec2 lowCorner, vec2 highCorner, vec2 pos) {
    vec2 m = step(lowCorner, pos) * step(pos, highCorner);
    return int(m.x * m.y);
  }

  int rectOutlineMask(vec2 lowCorner, vec2 highCorner, float width, vec2 pos) {
    int m1 = rectMask(lowCorner, highCorner, pos);
    int m2 = rectMask(lowCorner + vec2(width), highCorner - vec2(width), pos);
    return m1 - m2;
  }

  int circleMask(vec2 center, float radius, vec2 pos) {
    vec2 distV = pos - center;
    float distSquare = dot(distV, distV);
    return int(step(distSquare, radius * radius));
  }

  vec4 square() {
    vec2 pos = gl_FragCoord.xy / uResolution;
    vec2 mouseDist = abs(uMouseRel - vec2(0.5));
    float size = max(mouseDist.x, mouseDist.y);

    float mask =
      float(rectOutlineMask(vec2(0.5 - size), vec2(0.5 + size), 0.001, pos));
    return vec4(vec3(mask), 1.0);
  }

  vec4 mondrian() {
    const vec4 BLACK = vec4(0.067, 0.086, 0.110, 1.0);
    const vec4 BLUE = vec4(0.400, 0.369, 0.604, 1.0);
    const vec4 RED = vec4(0.639, 0.122, 0.137, 1.0);
    const vec4 WHITE = vec4(0.961, 0.929, 0.863, 1.0);
    const vec4 YELLOW = vec4(0.988, 0.776, 0.192, 1.0);
    vec2 pos = gl_FragCoord.xy / uResolution;

    int redMask =
      rectMask(vec2(0.0, 0.65107), vec2(0.06164, 0.80897), pos) +
      rectMask(vec2(0.08904, 0.65107), vec2(0.20776, 0.80897), pos) +
      rectMask(vec2(0.0, 0.84600), vec2(0.06164, 1.0), pos) +
      rectMask(vec2(0.08904, 0.84600), vec2(0.20776, 1.0), pos);
    int yellowMask =
      rectMask(vec2(0.97489, 0.65107), vec2(1.0, 0.80897), pos) +
      rectMask(vec2(0.97489, 0.84600), vec2(1.0, 1.0), pos);
    int blueMask =
      rectMask(vec2(0.76028, 0.0), vec2(0.94749, 0.07212), pos) +
      rectMask(vec2(0.97489, 0.0), vec2(1.0, 0.07212), pos);
    int whiteMask =
      rectMask(vec2(0.23516, 0.0), vec2(0.73288, 0.07212), pos) +
      rectMask(vec2(0.0, 0.0), vec2(0.20776, 0.62183), pos) +
      rectMask(vec2(0.23516, 0.09357), vec2(0.73288, 0.62183), pos) +
      rectMask(vec2(0.76028, 0.09357), vec2(0.94749, 0.62183), pos) +
      rectMask(vec2(0.97489, 0.09357), vec2(1.0, 0.62183), pos) +
      rectMask(vec2(0.23516, 0.65107), vec2(0.73288, 0.80897), pos) +
      rectMask(vec2(0.76028, 0.65107), vec2(0.94749, 0.80897), pos) +
      rectMask(vec2(0.23516, 0.84600), vec2(0.73288, 1.0), pos) +
      rectMask(vec2(0.76028, 0.84600), vec2(0.94749, 1.0), pos);
    int blackMask = 1 - redMask * yellowMask * blueMask * whiteMask;

    vec4 color =
      vec4(redMask) * RED + vec4(yellowMask) * YELLOW +
      vec4(blueMask) * BLUE + vec4(whiteMask) * WHITE + vec4(blackMask) * BLACK;
    return color;
  }

  vec4 circle() {
    const vec4 COLOR1 = vec4(0.765, 0.871, 0.898, 1.0);
    const vec4 COLOR2 = vec4(0.0, 0.231, 0.271, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;
    float fragDist = length(pos - vec2(0.5));
    float mouseDist = length(uMouseRel - vec2(0.5));

    float weight =
      circleMask(vec2(0.5), mouseDist, pos) == 0 ? 1.0 : fragDist * 1.7;
    return mix(COLOR1, COLOR2, weight);
  }

  vec4 ripple() {
    const vec4 COLOR1 = vec4(0.953, 0.800, 0.435, 1.0);
    const vec4 COLOR2 = vec4(0.871, 0.475, 0.129, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;

    vec2 r = pos * 2.0 - 1.0;
    vec2 m = abs(uMouseRel * 2.0 - 1.0);
    float d = length(abs(r) - m);
    // float d = length(min(abs(r) - m, 0.0));
    // float d = length(max(abs(r) - m, 0.0));
    float w = fract((d - uTimeMillis / 10000.0) * 7.0);

    return mix(COLOR1, COLOR2, w);
  }

  vec4 daisy() {
    vec4 BG_COLOR = vec4(0.980, 0.396, 0.255, 1.0);
    vec4 FG_COLOR_1 = vec4(0.216, 0.365, 0.588, 1.0);
    vec4 FG_COLOR_2 = vec4(1.0, 0.729, 0.0, 1.0);
    vec2 pos = gl_FragCoord.xy / uResolution;

    vec2 polar = pos - vec2(0.5);
    float dist = length(polar);
    float angle = atan(polar.y, polar.x);

    float f = cos(angle * 5.0 + uTimeMillis / 2000.0) * 0.45;
    float fPos = max(f, 0.0);
    float fNeg = max(-f, 0.0);

    float w1 = step(0.0, fPos) - step(fPos, dist);
    float w2 = step(0.0, fNeg) - step(fNeg, dist);
    float w3 = 1.0 - w1 - w2;

    return FG_COLOR_1 * w1 + FG_COLOR_2 * w2 + BG_COLOR * w3;
  }

  vec4 spin() {
    const vec4 COLOR1 = vec4(0.0, 0.161, 0.235, 1.0);
    const vec4 COLOR2 = vec4(0.945, 0.949, 0.804, 1.0);
    const vec4 COLOR3 = vec4(0.961, 0.161, 0.0, 1.0);

    vec2 pos = gl_FragCoord.xy / uResolution;

    vec2 polar = pos - vec2(0.5);
    float dist = length(polar);
    float angle = atan(polar.y, polar.x);
    float angle1 = angle / (2.0 * PI) + 0.5;

    // Segmented ring.
    float sr_a = angle * 6.0 + uTimeMillis / 1000.0;
    float sr_cos = clamp(cos(sr_a), -0.2, 0.2);
    float sr_f1 = sr_cos * 0.2 + 0.3;
    float sr_f2 = -sr_cos * 0.2 + 0.3;

    float sr_w1 = step(0.0, sr_f1) - step(sr_f1, dist);
    float sr_w2 = step(0.0, sr_f2) - step(sr_f2, dist);
    float sr_w = clamp(sr_w1 - sr_w2, 0.0, 1.0);

    // Inner circles.
    float ic_a = fract(angle1 * 6.0 - uTimeMillis / 413.4);
    vec2 ic_p = vec2(cos(ic_a), sin(ic_a)) * dist * 0.7;
    float ic_d = length(ic_p - vec2(0.1));
    float ic_w = step(ic_d, 0.03);

    float bg_w = 1.0 - min(sr_w + ic_w, 1.0);

    return COLOR1 * bg_w + COLOR2 * sr_w + COLOR3 * ic_w;
  }

  vec4 regularPolygons() {
    vec2 pos = gl_FragCoord.xy / uResolution;
    pos = pos * 2.0 - 1.0;

    int n = 3 + int(mod(uTimeMillis / 1000.0, 10.0));
    float r = TWO_PI / float(n);
    float a = atan(pos.y, pos.x) + PI + uTimeMillis / 1200.0;

    float d = cos(floor(0.5 + a / r) * r - a) * length(pos);
    float w = step(d, 0.4);

    return vec4(vec3(w), 1.0);
  }

  void main() {
    if (uShape == 1) {
      gl_FragColor = square();
    } else if (uShape == 2) {
      gl_FragColor = mondrian();
    } else if (uShape == 3) {
      gl_FragColor = circle();
    } else if (uShape == 4) {
      gl_FragColor = ripple();
    } else if (uShape == 5) {
      gl_FragColor = daisy();
    } else if (uShape == 6) {
      gl_FragColor = spin();
    } else if (uShape == 7) {
      gl_FragColor = regularPolygons();
    } else {
      gl_FragColor = vec4(1.0);
    }
  }
`;

const VERTEX_SHADER_SOURCE = `
  precision mediump float;

  attribute vec4 aVertexPosition;

  void main() {
    gl_Position = aVertexPosition;
  }
`;

let buffers;
let programInfo;

function initBuffers(gl) {
  const positions = [
    -1.0, -1.0,
     1.0, -1.0,
     1.0, 1.0,
    -1.0, 1.0,
  ];
  const positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

  const indices = [
    0, 1, 2,
    0, 2, 3,
  ];
  const indexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(
    gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

  buffers = {
    indices: indexBuffer,
    position: positionBuffer,
  };
}

function initShaderProgram(gl) {
  programInfo =
    createShaderProgram(gl, VERTEX_SHADER_SOURCE, FRAGMENT_SHADER_SOURCE);
  programInfo.uniformLocations.shape =
    gl.getUniformLocation(programInfo.program, 'uShape');
}

export function init(gl) {
  initBuffers(gl);
  initShaderProgram(gl);
}

export function getProgramInfo() {
  return programInfo;
}

export function advanceState() {}

export function getSpec() {
  return {
    name: 'BoS: Shapes',
    params: [
      {
        name: 'shape',
        readableName: 'Shape',
        type: 'enum',
        enumValues: Object.keys(SHAPE_TYPE),
      }
    ]
  };
}

export function setParameters(gl, params) {
  gl.useProgram(programInfo.program);
  gl.uniform1i(
    programInfo.uniformLocations.shape, SHAPE_TYPE[params.shape]);
}

export function draw(gl, delta) {
  gl.useProgram(programInfo.program);

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.clearDepth(1.0);
  gl.enable(gl.DEPTH_TEST);
  gl.depthFunc(gl.LEQUAL);

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
  gl.vertexAttribPointer(
    programInfo.attribLocations.vertexPosition, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

  gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
}

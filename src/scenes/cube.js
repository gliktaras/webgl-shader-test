// Spinning cube from the MDN WebGL tutorial at
// https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Getting_started_with_WebGL.

import {createShaderProgram, loadTexture} from '../glUtils.js';

const FRAGMENT_SHADER_SOURCE = `
  precision mediump float;

  varying vec2 vTextureCoord;

  uniform vec2 uMouseRel;
  uniform sampler2D uSampler;

  void main() {
    vec4 texel = texture2D(uSampler, vTextureCoord);
    gl_FragColor = vec4(
      mix(texel.x, 1.0, uMouseRel.x), mix(texel.y, 1.0, uMouseRel.y), texel.zw);
  }
`;

const VERTEX_SHADER_SOURCE = `
  precision mediump float;

  attribute vec2 aTextureCoord;
  attribute vec4 aVertexPosition;

  uniform mat4 uModelViewMatrix;
  uniform mat4 uProjectionMatrix;

  varying vec2 vTextureCoord;

  void main() {
    gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
    vTextureCoord = aTextureCoord;
  }
`;

let rotation = 0;

let buffers;
let programInfo;
let textures;

function initBuffers(gl) {
  const positions = [
    // Front face
    -1.0, -1.0, 1.0,
     1.0, -1.0, 1.0,
     1.0, 1.0, 1.0,
    -1.0, 1.0, 1.0,
    // Back face
    -1.0, -1.0, -1.0,
    -1.0, 1.0, -1.0,
     1.0, 1.0, -1.0,
     1.0, -1.0, -1.0,
    // Top face
    -1.0, 1.0, -1.0,
    -1.0, 1.0, 1.0,
     1.0, 1.0, 1.0,
     1.0, 1.0, -1.0,
    // Bottom face
    -1.0, -1.0, -1.0,
     1.0, -1.0, -1.0,
     1.0, -1.0, 1.0,
    -1.0, -1.0, 1.0,
    // Right face
     1.0, -1.0, -1.0,
     1.0, 1.0, -1.0,
     1.0, 1.0, 1.0,
     1.0, -1.0, 1.0,
    // Left face
    -1.0, -1.0, -1.0,
    -1.0, -1.0, 1.0,
    -1.0, 1.0, 1.0,
    -1.0, 1.0, -1.0,
  ];
  const positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

  const textureCoords = [
    // Front
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
    // Back
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
    // Top
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
    // Bottom
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
    // Right
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
    // Left
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
  ];
  const textureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoords), gl.STATIC_DRAW);

  const indices = [
    // Front face
    0, 1, 2,
    0, 2, 3,
    // Back face
    4, 5, 6,
    4, 6, 7,
    // Top face
    8, 9, 10,
    8, 10, 11,
    // Bottom face
    12, 13, 14,
    12, 14, 15,
    // Right face
    16, 17, 18,
    16, 18, 19,
    // Left face
    20, 21, 22,
    20, 22, 23,
  ];
  const indexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(
    gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

  buffers = {
    indices: indexBuffer,
    position: positionBuffer,
    textureCoord: textureCoordBuffer,
  };
}

function initShaderProgram(gl) {
  programInfo =
    createShaderProgram(gl, VERTEX_SHADER_SOURCE, FRAGMENT_SHADER_SOURCE);

  programInfo.attribLocations.textureCoord =
    gl.getAttribLocation(programInfo.program, 'aTextureCoord');
  programInfo.attribLocations.vertexPosition =
    gl.getAttribLocation(programInfo.program, 'aVertexPosition');

  programInfo.uniformLocations.modelViewMatrix =
    gl.getUniformLocation(programInfo.program, 'uModelViewMatrix');
  programInfo.uniformLocations.projectionMatrix =
    gl.getUniformLocation(programInfo.program, 'uProjectionMatrix');
  programInfo.uniformLocations.sampler =
    gl.getUniformLocation(programInfo.program, 'uSampler');
}

function initTextures(gl) {
  textures = {
    cubeSurface: loadTexture(gl, 'textures/pavement.png'),
  };
}

export function init(gl) {
  initBuffers(gl);
  initShaderProgram(gl);
  initTextures(gl);
}

export function getProgramInfo() {
  return programInfo;
}

export function advanceState() {
  rotation++;
}

export function getSpec() {
  return {
    name: 'MDN: Cube',
    params: [],
  };
}

export function setParameters(params) {}

export function draw(gl, delta) {
  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.clearDepth(1.0);
  gl.enable(gl.DEPTH_TEST);
  gl.depthFunc(gl.LEQUAL);

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  const perspectiveMatrix = mat4.create();
  {
    const fov = 45.0 * Math.PI / 180.0;
    const aspectRatio = gl.canvas.clientWidth / gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    mat4.perspective(perspectiveMatrix, fov, aspectRatio, zNear, zFar);
  }

  const modelViewMatrix = mat4.create();
  mat4.translate(modelViewMatrix, modelViewMatrix, [-0.0, 0.0, -6.0]);
  mat4.rotate(
    modelViewMatrix, modelViewMatrix, (rotation + delta) * 0.01, [0.0, 0.0, 1.0]);
  mat4.rotate(
    modelViewMatrix, modelViewMatrix, (rotation + delta) * 0.007, [0.0, 1.0, 0.0]);
  mat4.rotate(
    modelViewMatrix, modelViewMatrix, (rotation + delta) * 0.003, [1.0, 0.0, 0.0]);

  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
  gl.vertexAttribPointer(
    programInfo.attribLocations.vertexPosition, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);

  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.textureCoord);
  gl.vertexAttribPointer(
    programInfo.attribLocations.textureCoord, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(programInfo.attribLocations.textureCoord);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, textures.cubeSurface);
  gl.uniform1i(programInfo.uniformLocations.sampler, 0);

  gl.uniformMatrix4fv(
    programInfo.uniformLocations.modelViewMatrix, false, modelViewMatrix);
  gl.uniformMatrix4fv(
    programInfo.uniformLocations.projectionMatrix, false, perspectiveMatrix);

  gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);
}

import * as colors from './scenes/colors.js';
import * as cube from './scenes/cube.js';
import * as noise from './scenes/noise.js';
import * as patterns from './scenes/patterns.js';
import * as random from './scenes/random.js';
import * as shapes from './scenes/shapes.js';
import * as shapingFunctions from './scenes/shapingFunctions.js';

const SCENES = [cube, shapingFunctions, colors, shapes, patterns, random, noise];

const TICK_LENGTH_MS = 1000.0 / 100.0;
const MAX_TICK_INCREMENT = TICK_LENGTH_MS * 5;

let mouseX = 0;
let mouseY = 0;
let loopRunning = false;

function initSceneControls(gl, scene) {
  let spec = scene.getSpec();

  let paramsEl = document.getElementById('scene-params');
  paramsEl.innerHTML = '';

  let sceneParams = {};
  for (let i = 0; i < spec.params.length; i++) {
    let param = spec.params[i];

    var labelEl = document.createElement('label');
    labelEl.innerHTML = param.readableName + ': ';
    paramsEl.appendChild(labelEl);

    if (param.type === 'enum') {
      let selectEl = document.createElement('select');
      for (let j = 0; j < param.enumValues.length; j++) {
        let optionEl = document.createElement('option');
        optionEl.text = param.enumValues[j];
        optionEl.value = param.enumValues[j];
        selectEl.add(optionEl);
      }
      selectEl.onchange = function () {
        sceneParams[param.name] =
          selectEl.options[selectEl.selectedIndex].value;
        scene.setParameters(gl, sceneParams);
      }
      labelEl.appendChild(selectEl);
      sceneParams[param.name] = param.enumValues[0];
    }
  }
  scene.setParameters(gl, sceneParams);
}

function renderLoop(scene) {
  const canvas = document.getElementById('gl-canvas');
  const gl = canvas.getContext('webgl');
  if (!gl) {
    alert('WebGL not supported.');
    return;
  }

  scene.init(gl);
  initSceneControls(gl, scene);
  const programInfo = scene.getProgramInfo();

  let previousTime = performance.now();
  let firstTime = previousTime;
  let delta = 0.0;

  let firstRender = true;

  function render(currentTime) {
    if (!loopRunning) {
      return;
    }

    gl.useProgram(programInfo.program);
    if (gl.canvas.width !== gl.canvas.clientWidth
        || gl.canvas.height !== gl.canvas.clientHeight
        || firstRender) {
      firstRender = false;

      gl.canvas.width = gl.canvas.clientWidth;
      gl.canvas.height = gl.canvas.clientHeight;
      gl.viewport(0, 0, gl.canvas.clientWidth, gl.canvas.clientHeight);
      gl.uniform2f(
        programInfo.uniformLocations.resolution, gl.canvas.clientWidth,
        gl.canvas.clientHeight);
    }

    delta += Math.min(currentTime - previousTime, MAX_TICK_INCREMENT);
    previousTime = currentTime;

    gl.uniform1f(
      programInfo.uniformLocations.timeMillis, currentTime - firstTime);
    gl.uniform2f(programInfo.uniformLocations.mouseAbsolute, mouseX, mouseY);
    gl.uniform2f(
      programInfo.uniformLocations.mouseRelative, mouseX / gl.canvas.clientWidth,
      mouseY / gl.canvas.clientHeight);

    while (delta >= TICK_LENGTH_MS) {
      scene.advanceState();
      delta -= TICK_LENGTH_MS;
    }
    scene.draw(gl, delta / TICK_LENGTH_MS);
    window.requestAnimationFrame(render);
  }

  loopRunning = true;
  window.requestAnimationFrame(render);
}

function initCanvas () {
  const canvas = document.getElementById('gl-canvas');
  canvas.onmousemove = function (event) {
    mouseX = event.clientX;
    mouseY = event.clientY;
  };
}

function setScene (sceneIndex) {
  document.getElementById('scene-name').selectedIndex = sceneIndex;
  loopRunning = false;
  setTimeout(function () { renderLoop(SCENES[sceneIndex]); }, 0);
}

function initSceneSelect () {
  let sceneSelectEl = document.getElementById('scene-name');
  for (let i = 0; i < SCENES.length; i++) {
    let spec = SCENES[i].getSpec();
    let optionEl = document.createElement('option');
    optionEl.text = spec.name;
    optionEl.value = i;
    sceneSelectEl.add(optionEl);
  }

  sceneSelectEl.onchange = function () {
    setScene(sceneSelectEl.options[sceneSelectEl.selectedIndex].value);
  }
}

function main() {
  initCanvas();
  initSceneSelect();
  setScene(6);
}

window.onload = main;
